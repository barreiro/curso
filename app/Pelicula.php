<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
	protected $fillable = ['titulo', 'ano', 'identificador', 'poster', 'productora_id'];

	public function productora()
	{
		return $this->belongsTo('App\Productor', 'productora_id');
	}

	public function generos() {
		return $this->belongsToMany('App\Genero');
	}

    public function scopeRockySaga($query) {
    	return $query->where('identificador', 'LIKE', '%rocky%')->orWhere('identificador', 'LIKE', '%creed%');
    }

    public function getRouteKeyName() {
    	return 'identificador';
    }

    public static function generateUniqueSlug($titulo) {
    	$originalSlug = str_slug($titulo);

    	$i = 1;
    	$slug = $originalSlug;
    	$exists = Pelicula::where('identificador', $slug)->count();

    	while ($exists) {
    		$slug = $originalSlug . '-' . $i;
    		$exists = Pelicula::where('identificador', $slug)->count();

    		$i++;
    	}

    	return $slug;
    }
}
