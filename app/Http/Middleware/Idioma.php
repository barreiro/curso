<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Config;

class Idioma
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->route('locale');

        if (!in_array($locale, Config::get('app.available_locales'))) {
            abort(404);
        }

        App::setLocale($locale);

        return $next($request);
    }
}
