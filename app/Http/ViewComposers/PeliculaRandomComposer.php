<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Pelicula;

class PeliculaRandomComposer {

	public function compose(View $view) {
		$pelicula = \App\Pelicula::inRandomOrder()->first();

		$view->with('pelicula', $pelicula);
	}
	
}