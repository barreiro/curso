<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Pelicula;

class FichaPelicula extends Mailable
{
    use Queueable, SerializesModels;

    public $pelicula;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Pelicula $pelicula)
    {
        $this->pelicula = $pelicula;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->pelicula->poster) {
            $this->attach(public_path() . $this->pelicula->poster);
        }
        return $this->view('mail.pelicula');
    }
}
