<?php

return [
	'editar' => 'Editar',
	'ano' => 'Año',
	'titulo' => 'Título',
	'peliculas' => [
		'cabecera' => 'Películas',
		'crear' => 'Crear película',
	],
];