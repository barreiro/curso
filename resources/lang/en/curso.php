<?php

return [
	'editar' => 'Edit',
	'ano' => 'Year',
	'titulo' => 'Title',
	'peliculas' => [
		'cabecera' => 'Movies',
		'crear' => 'Create movie',
	],
];