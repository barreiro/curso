<!doctype html>
<html lang="es">
<head>
 <meta charset="UTF-8">
 <title>{{$title}}</title>
 @section ('estilos')
 	<link rel="stylesheet" href="{{ mix('/css/app.css') }}" />
 @show
</head>
<body>
	@include('cabecera', ['title2' => $title])

	@yield('body')



	@include('pie')

	@yield('body2')
	<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>