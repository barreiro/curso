@extends('layouts.base', ['title' => 'Formulario'])

@section('body')
<p>
	<a href="{{ route('peliculas.index') }}">Peliculas de accion</a>
</p>

<div class="circulo"></div>
<div class="circulo grande"></div>

	<h1>Introducir película</h1>

	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<form action="{{ url('/') }}" method="POST" enctype="multipart/form-data" >
		{{csrf_field()}}
		<p>
			<label for="titulo">Título</label>
			<input type="text" id="titulo" name="titulo" placeholder="Título" value="{{old('titulo')}}" />
		</p>
		<p>
			<label for="ano">Año</label>
			<input type="number" id="ano" name="ano" placeholder="Año" value="{{old('ano')}}" />
		</p>

		<p>
			<label for="poster">Poster</label>
			<input type="file" id="poster" name="poster"  />
		</p>

		<p>
			<button>Enviar</button>
		</p>
	</form>
@stop