@extends('layouts.app', ['title' => 'Peliculas'])

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('curso.peliculas.cabecera')</div>

                <div class="panel-body">
                    <table>
						<thead>
							<tr>
								<td>@lang('curso.titulo')</td>
								<td>@lang('curso.ano')</td>
								@guest
								@else
								<td>@lang('curso.editar')</td>
								@endguest
							</tr>
						</thead>
						<tbody>
							@forelse ($peliculas as $pelicula)
								<tr>
									<td><a href="{{route('peliculas.show', ['peliculas' => $pelicula])}}">{{$pelicula->titulo}}</a></td>
									<td>{{$pelicula->ano}}</td>
									@auth
									<td><a href="{{route('peliculas.edit', $pelicula)}}">@lang('curso.editar')</a></td>
									@endauth
								</tr>
							@empty
								No hay películas
							@endforelse
						</tbody>
					</table>

					{{$peliculas->links()}}

					@auth
					<a href="{{route('peliculas.create')}}">@lang('curso.peliculas.crear')</a>
					@endauth
                </div>
            </div>
        </div>
    </div>
</div>

@include('peliculas.random')
	
@stop