@extends('layouts.app', ['title' => 'Formulario'])

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Editar película {{$pelicula->titulo}}</div>

                <div class="panel-body">
                    @if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{$error}}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form action="{{ route('peliculas.update', $pelicula) }}" method="POST" enctype="multipart/form-data" >
						{{csrf_field()}}
						{{method_field('PUT')}}

						<p>
							<label for="titulo">Título</label>
							<input type="text" id="titulo" name="titulo" placeholder="Título" value="{{old('titulo', $pelicula->titulo)}}" />
						</p>
						<p>
							<label for="ano">Año</label>
							<input type="number" id="ano" name="ano" placeholder="Año" value="{{old('ano', $pelicula->ano)}}" />
						</p>

						<p>
							<label for="poster">Poster</label>
							@if ($pelicula->poster)
								<img src="{{url($pelicula->poster)}}" />
							@endif
							<input type="file" id="poster" name="poster"  />
						</p>

						<p>
							<label for="productora_id">Productora</label>
							<select id="productora_id" name="productora_id">
								@if ($pelicula->productora)
									<option selected disabled>Seleccionar productora</option>
								@endif
								@foreach ($productoras as $productora)
									<option value="{{$productora->id}}" 
										@if (old('productora_id', $pelicula->productora_id)==$productora->id) 
											selected 
										@endif 
										> {{$productora->nombre}} </option>
								@endforeach
							</select>
						</p>

						<p>
							<label for="generos">Generos</label>
							<select id="generos" name="generos[]" multiple>
								@foreach ($generos as $genero)
									<option value="{{$genero->id}}" 
				@if (in_array($genero->id, old('generos', $pelicula->generos->pluck('id')->toArray())))
											selected
										@endif
										> {{$genero->nombre}} </option>
								@endforeach
							</select>
						</p>

						<p>
							<button>Enviar</button>
						</p>
					</form>

					<form action="{{route('peliculas.destroy', $pelicula)}}" method="POST">
						{{csrf_field()}}
						{{method_field('DELETE')}}
						<p>
							<input type="submit" value="Eliminar" />
						</p>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>


	

@stop