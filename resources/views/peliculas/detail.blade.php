@extends('layouts.app', ['title' => 'Pelicula' . $pelicula->titulo])

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Pelicula: {{$pelicula->titulo}}</div>

                <div class="panel-body">
                    <h3>Año: {{$pelicula->ano}}</h3>

					@if ($pelicula->poster)
						<img src="{{url($pelicula->poster)}}" />
					@endif

					@if ($pelicula->productora)
						<p>
							Productora: {{$pelicula->productora->nombre}}
						</p>
					@endif

					@if ($pelicula->generos && $pelicula->generos->count()>0)
						<p>
							Géneros: {{$pelicula->generos->implode('nombre', ', ')}}
						</p>
					@endif

					<p>
						<a href="{{route('peliculas.edit', $pelicula)}}">Editar película</a>
					</p>
                </div>
            </div>
        </div>
    </div>
</div>

@include('peliculas.random')
	
@stop