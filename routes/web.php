<?php

use \App\Pelicula;
use \App\Productor;
use \App\Genero;
use \App\Mail\FichaPelicula;

Route::get('/email/enviar', function () {
	$pelicula = Pelicula::first();

	Mail::to('carlos.barreiro@imaginanet.com')->send(new FichaPelicula($pelicula));

	return "Enviado";
});

Route::get('/email/previsualizar', function () {
	$pelicula = Pelicula::first();

	return new FichaPelicula($pelicula);
});

Route::get('/email', function () {
		Mail::raw('Que tal te va', function ($message) {
			$message->subject('Hola')->to('carlos.barreiro@imaginanet.com');
		});

		return "enviado";
	})->name('email');

Route::get('/creartoken', function () {

	$user = Auth::user();

	$token = $user->createToken('El nombre que queramos')->accessToken;

	return $token;

})->middleware('auth');

Route::get('/pruebapeliculas', function () {
	return Pelicula::all();
})->name('nombre');

Route::middleware('auth')->prefix('pruebas')->group(function () {

	Route::get('/nombre', function () {
		return Auth::user()->name;
	})->name('nombre');

	

});

Route::get('/test', function () {
 $pelicula = new Pelicula;
 $pelicula->identificador = "rambo";
 $pelicula->titulo = "Rambo";
 $pelicula->ano = 1982;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rambo-ii";
 $pelicula->titulo = "Rambo II";
 $pelicula->ano = 1985;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rambo-iii";
 $pelicula->titulo = "Rambo III";
 $pelicula->ano = 1988;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "john-rambo";
 $pelicula->titulo = "John Rambo";
 $pelicula->ano = 2008;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rocky";
 $pelicula->titulo = "Rocky";
 $pelicula->ano = 1976;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rocky-ii";
 $pelicula->titulo = "Rocky II";
 $pelicula->ano = 1979;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rocky-iii";
 $pelicula->titulo = "Rocky III";
 $pelicula->ano = 1982;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rocky-iv";
 $pelicula->titulo = "Rocky IV";
 $pelicula->ano = 1986;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rocky-v";
 $pelicula->titulo = "Rocky V";
 $pelicula->ano = 1990;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rocky-balboa";
 $pelicula->titulo = "Rocky Balboa";
 $pelicula->ano = 2006;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "creed";
 $pelicula->titulo = "Creed";
 $pelicula->ano = 2016;
 $pelicula->save();
});

Route::get('/test2', function () {
 $productor = new Productor;
 $productor->nombre = "Sony";
 $productor->save();
 $productor = new Productor;
 $productor->nombre = "Disney";
 $productor->save();
 $productor = new Productor;
 $productor->nombre = "Fox";
 $productor->save();
 $productor = new Productor;
 $productor->nombre = "Warner Bros";
 $productor->save();
 $productor = new Productor;
 $productor->nombre = "MGM";
 $productor->save();
 $genero = new Genero;
 $genero->identificador = 'accion';
 $genero->nombre = 'Acción';
 $genero->save();
 $genero = new Genero;
 $genero->identificador = 'comedia';
 $genero->nombre = 'Comedia';
 $genero->save();
 $genero = new Genero;
 $genero->identificador = 'drama';
 $genero->nombre = 'Drama';
 $genero->save();
 $genero = new Genero;
 $genero->identificador = 'terror';
 $genero->nombre = 'Terror';
 $genero->save();
});

Route::get('/{locale}/peliculas', 'PeliculaController@index')->middleware('idioma');

Route::resource('peliculas', 'PeliculaController');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
